<?php
namespace HIVE\HiveExtFeloginTracking\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class LogTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtFeloginTracking\Domain\Model\Log
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtFeloginTracking\Domain\Model\Log();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getUsergroupReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setUsergroupForIntSetsUsergroup()
    {
    }

    /**
     * @test
     */
    public function getUsergroupTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getUsergroupTitle()
        );
    }

    /**
     * @test
     */
    public function setUsergroupTitleForStringSetsUsergroupTitle()
    {
        $this->subject->setUsergroupTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'usergroupTitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getUserReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setUserForIntSetsUser()
    {
    }

    /**
     * @test
     */
    public function getUserTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getUserTitle()
        );
    }

    /**
     * @test
     */
    public function setUserTitleForStringSetsUserTitle()
    {
        $this->subject->setUserTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'userTitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPageReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setPageForIntSetsPage()
    {
    }

    /**
     * @test
     */
    public function getPagePathReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPagePath()
        );
    }

    /**
     * @test
     */
    public function setPagePathForStringSetsPagePath()
    {
        $this->subject->setPagePath('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'pagePath',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAccessReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getAccess()
        );
    }

    /**
     * @test
     */
    public function setAccessForDateTimeSetsAccess()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setAccess($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'access',
            $this->subject
        );
    }
}
