<?php
namespace HIVE\HiveExtFeloginTracking\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_felogin_tracking" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * Log
 */
class Log extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * usergroup
     *
     * @var int
     */
    protected $usergroup = '';

    /**
     * usergroupTitle
     *
     * @var string
     */
    protected $usergroupTitle = '';

    /**
     * user
     *
     * @var int
     */
    protected $user = '';

    /**
     * userTitle
     *
     * @var string
     */
    protected $userTitle = '';

    /**
     * access
     *
     * @var \DateTime
     */
    protected $access = null;

    /**
     * page
     *
     * @var int
     */
    protected $page = 0;

    /**
     * pagePath
     *
     * @var string
     */
    protected $pagePath = '';

    /**
     * Returns the access
     *
     * @return \DateTime $access
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Sets the access
     *
     * @param \DateTime $access
     * @return void
     */
    public function setAccess(\DateTime $access)
    {
        $this->access = $access;
    }

    /**
     * Returns the usergroup
     *
     * @return int usergroup
     */
    public function getUsergroup()
    {
        return $this->usergroup;
    }

    /**
     * Sets the usergroup
     *
     * @param string $usergroup
     * @return void
     */
    public function setUsergroup($usergroup)
    {
        $this->usergroup = $usergroup;
    }

    /**
     * Returns the usergroupTitle
     *
     * @return string usergroupTitle
     */
    public function getUsergroupTitle()
    {
        return $this->usergroupTitle;
    }

    /**
     * Sets the usergroupTitle
     *
     * @param string $usergroupTitle
     * @return void
     */
    public function setUsergroupTitle($usergroupTitle)
    {
        $this->usergroupTitle = $usergroupTitle;
    }

    /**
     * Returns the user
     *
     * @return int user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the user
     *
     * @param string $user
     * @return void
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Returns the userTitle
     *
     * @return string userTitle
     */
    public function getUserTitle()
    {
        return $this->userTitle;
    }

    /**
     * Sets the userTitle
     *
     * @param string $userTitle
     * @return void
     */
    public function setUserTitle($userTitle)
    {
        $this->userTitle = $userTitle;
    }

    /**
     * Returns the page
     *
     * @return int $page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Sets the page
     *
     * @param int $page
     * @return void
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * Returns the pagePath
     *
     * @return string $pagePath
     */
    public function getPagePath()
    {
        return $this->pagePath;
    }

    /**
     * Sets the pagePath
     *
     * @param string $pagePath
     * @return void
     */
    public function setPagePath($pagePath)
    {
        $this->pagePath = $pagePath;
    }
}
