<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtFeloginTracking',
            'Hiveextfelogintrackinglog',
            'hive_ext_felogin_tracking :: log'
        );

        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'HIVE.HiveExtFeloginTracking',
                'web', // Make module a submodule of 'web'
                'hiveextfelogintracking', // Submodule key
                '', // Position
                [
                    'Log' => 'list',
                    
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:hive_ext_felogin_tracking/Resources/Public/Icons/user_mod_hiveextfelogintracking.svg',
                    'labels' => 'LLL:EXT:hive_ext_felogin_tracking/Resources/Private/Language/locallang_hiveextfelogintracking.xlf',
                ]
            );

        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_felogin_tracking', 'Configuration/TypoScript', 'hive_ext_felogin_tracking');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextfelogintracking_domain_model_log', 'EXT:hive_ext_felogin_tracking/Resources/Private/Language/locallang_csh_tx_hiveextfelogintracking_domain_model_log.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextfelogintracking_domain_model_log');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder